package az.course.tdd.tictactoe.service;

import az.course.tdd.tictactoe.exception.BadRequestException;
import az.course.tdd.tictactoe.exception.StudentNotFoundException;
import az.course.tdd.tictactoe.model.Student;
import az.course.tdd.tictactoe.repository.StudentRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        List<Student> students = studentRepository.findAll();
        return students.stream().map(student -> {
            student.setId(1);
            student.setName(student.getName());
            student.setEmail(student.getEmail());
            return student;
        }).collect(Collectors.toList());
    }

    public void addStudent(Student student) {
        Boolean existsEmail = studentRepository
                .existsByEmail(student.getEmail());
        if (existsEmail) {
            throw new BadRequestException(
                    "Email " + student.getEmail() + " taken");
        }

        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        Student student=studentRepository.findById(studentId).orElseThrow(StudentNotFoundException::new);
        studentRepository.delete(student);
    }
}

