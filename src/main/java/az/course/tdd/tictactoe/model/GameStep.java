package az.course.tdd.tictactoe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class GameStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Character player;
    private int x;
    private int y;

}
