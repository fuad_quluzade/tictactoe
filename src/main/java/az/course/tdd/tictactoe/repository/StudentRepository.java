package az.course.tdd.tictactoe.repository;

import az.course.tdd.tictactoe.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    Boolean existsByEmail(String email);
}
