package az.course.tdd.tictactoe.service;


import az.course.tdd.tictactoe.exception.BadRequestException;
import az.course.tdd.tictactoe.exception.StudentNotFoundException;
import az.course.tdd.tictactoe.model.GameStep;
import az.course.tdd.tictactoe.model.Student;
import az.course.tdd.tictactoe.repository.StudentRepository;
import lombok.Builder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    private StudentService studentService;

    @Mock
    private StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        studentService = new StudentService(studentRepository);
    }

    @Test
    void canGetAllStudents() {
        //Arrange
        when(studentRepository.findAll()).thenReturn(List.of(new Student(1, "F", "Q", "@")));
        //Act
        List<Student> allStudents = studentService.getAllStudents();
        //assert
        assertThat(allStudents).isEqualTo(List.of(new Student(1, "F", "Q", "@")));
    }

    @Test
    void canAddStudents() {
        //Arrange
        Student student = prepareStudent();
        //Act

        studentService.addStudent(student);
        //Assert
        verify(studentRepository).save(any());
    }

    @Test
    void willThrowExceptionWhenEmailIsTaken() {
        //Arrange
        Student student =prepareStudent();

        when(studentRepository.existsByEmail(student.getEmail())).thenReturn(true);
        //Act
        //Assert
        assertThatThrownBy(() -> studentService.addStudent(student))
                .isInstanceOf(BadRequestException.class)
                .hasMessage("Email " + student.getEmail() + " taken");
        verify(studentRepository,never()).save(student);
    }

    @Test
    void ifStudentDoesNotExistByIdThenThrowException(){
        //Arrange
        Student student = prepareStudent();
        when(studentRepository.findById(student.getId())).thenReturn(Optional.empty());
           assertThatThrownBy(() -> studentService.deleteStudent(student.getId()))
                .isInstanceOf(StudentNotFoundException.class)
                .hasMessage("Student not found");
        verify(studentRepository,times(0)).delete(student);
    }


    private Student prepareStudent(){
        return Student.builder()
                .id(1l)
                .name("Fuad")
                .surname("Quluzade")
                .email("F@G.Com")
                .build();
    }
}