package az.course.tdd.tictactoe.spec;

import az.course.tdd.tictactoe.model.GameStep;
import az.course.tdd.tictactoe.repository.GameStepRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TicTacToe {

    private final GameStepRepository repository;

    private static final int SIZE = 3;
    private char[][] board = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
    };

    private char lastPlayer = '\0';

    public String play(int x, int y) {
        checkXAxis(x);
        checkYAxis(y);
        lastPlayer = nextPlayer();
        saveToDb(lastPlayer, x, y);
        checkIfBoardIsOccupied(x, y, lastPlayer);

//        printTheBoard();
        if (isWin(x, y)) {
            return lastPlayer + " is winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "no winner";
        }
    }

    private void checkYAxis(int y) {
        if (y < 1 || y > 3) {
            throw new RuntimeException("Y is outside the board");
        }
    }

    private void checkXAxis(int x) {
        if (x < 1 || x > 3) {
            throw new RuntimeException("X is outside the board");
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return 'O';
        }
        return 'X';
    }

    public List<GameStep> getGameSteps() {
        List<GameStep> all = repository.findAll();
        return all.stream().map(item -> {
            item.setX(item.getX() + 1);
            item.setY(item.getY() + 1);
            return item;
        }).collect(Collectors.toList());
    }
    public GameStep getGameStepDetails(Long id) {
       return repository.findById(id).get();

    }

    private void printTheBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }

    }

    private void checkIfBoardIsOccupied(int x, int y, char lastPlayer) {
        if (board[x - 1][y - 1] != '-') {
            throw new RuntimeException("board is occupied");
        } else {
            board[x - 1][y - 1] = lastPlayer;
        }
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * 3;
        char horizontal, vertical, diagonal1, diagonal2;
        horizontal = vertical = diagonal1 = diagonal2 = '\0';
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
        }
        if (horizontal == playerTotal
                || vertical == playerTotal
                || diagonal1 == playerTotal
                || diagonal2 == playerTotal) {
            return true;
        }
        return false;
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private void saveToDb(Character player, int x, int y) {
        GameStep gameStep = new GameStep();
        gameStep.setPlayer(player);
        gameStep.setX(x);
        gameStep.setY(y);
        repository.save(gameStep);
    }
}
