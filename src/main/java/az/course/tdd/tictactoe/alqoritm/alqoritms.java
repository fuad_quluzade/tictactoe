package az.course.tdd.tictactoe.alqoritm;

public class alqoritms {
    public static void main(String[] args) {
         String word="programming";
         freqCount(word);

    }

    public static void freqCount(String str){
       str=str.replace(" ","");
       while (str.length()>0){
           char ch=str.charAt(0);
           System.out.println(ch+" --- " + countChar(str,ch));
           str=str.replace(""+ch,"");
       }
    }

    private static int countChar(String str, char ch) {
        int count=0;
        while (str.indexOf(ch) != -1){
            count++;
            str=str.substring(str.indexOf(ch)+1);
        }
        return count;
    }
}
