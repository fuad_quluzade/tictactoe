package az.course.tdd.tictactoe;

import az.course.tdd.tictactoe.model.GameStep;
import az.course.tdd.tictactoe.repository.GameStepRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
public class GameRepositoryFake implements GameStepRepository {
    @Override
    public List<GameStep> findAll() {
        return null;
    }

    @Override
    public List<GameStep> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<GameStep> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<GameStep> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(GameStep entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends GameStep> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends GameStep> S save(S entity) {
        log.info("Hello Fake ...");
        return null;
    }

    @Override
    public <S extends GameStep> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<GameStep> findById(Long aLong) {
        if(aLong==1){
            return Optional.of(new GameStep(1,'X',1,1));
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends GameStep> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends GameStep> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<GameStep> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public GameStep getOne(Long aLong) {
        return null;
    }

    @Override
    public GameStep getById(Long aLong) {
        return null;
    }

    @Override
    public <S extends GameStep> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends GameStep> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends GameStep> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends GameStep> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends GameStep> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends GameStep> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends GameStep, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
