package az.course.tdd.tictactoe.validations;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PhoneNumberValidationsTest {

    PhoneNumberValidation validation;

    @BeforeEach
    void setUp() {
        validation=new PhoneNumberValidation();
    }

    @ParameterizedTest
    @CsvSource({
            "+994550000000,true",
            "994550000000,false",
            "99455000000000,false"
    })
    void itShouldBeValidationPhoneNumber(String phoneNumber,boolean expect) {

        boolean isValid=validation.test(phoneNumber);

        assertThat(isValid).isEqualTo(expect);
    }

//    @Test
//    @DisplayName("Should be fail when is not start plus")
//    void itShouldValidatePhoneNumberWhenDoesNotStartWithPlus(){
//        String phoneNumber="994550000000";
//        boolean isValid=validation.test(phoneNumber);
//        assertThat(isValid).isFalse();
//    }
//
//    @Test
//    @DisplayName("Should fail when length bigger than 13")
//    void itShouldBeValidatePhoneNumberWhenLengthBiggerThan13(){
//        String phoneNumber="99455000000000";
//        boolean isValid=validation.test(phoneNumber);
//        assertThat(isValid).isFalse();
//    }
}
