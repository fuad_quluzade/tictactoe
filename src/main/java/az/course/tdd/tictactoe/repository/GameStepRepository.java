package az.course.tdd.tictactoe.repository;

import az.course.tdd.tictactoe.model.GameStep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameStepRepository extends JpaRepository<GameStep,Long> {
}
