package az.course.tdd.tictactoe.exception;

public class StudentNotFoundException extends RuntimeException{
    public static final String MESSAGE = "Student not found";
    public StudentNotFoundException(String msg) {
        super(msg);
    }
    public StudentNotFoundException() {
        super(MESSAGE);
    }

}
