package az.course.tdd.tictactoe.contract;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CaseInsensitiveString {
    private final String s;

    public CaseInsensitiveString(String s) {
        if (s == null) {
            throw new NullPointerException();
        } else
            this.s = s;
    }
    @Override
    public boolean equals(Object o) {
        if (o instanceof CaseInsensitiveString)  // obyekt CaseInsensitiveString instansidirsa
            return s.equalsIgnoreCase(
                    ((CaseInsensitiveString) o).s);
        if (o instanceof String) // One-way  interoperability! object stringin instansidirsa , yuxardaki s -i  muqayise et Stringin instanci ile
        return s.equalsIgnoreCase((String) o);
        return false;
    }

    public static void main(String[] args) {
     CaseInsensitiveString caseInsensitiveString=new CaseInsensitiveString("Polish");
     String s="polish";
     boolean b=caseInsensitiveString.equals(s); // symmetric //liscov subtutition  pozulur, ekside true olmali idi
     boolean b2=s.equals(caseInsensitiveString); //symmetric // burda s Stringin icindeki equals metodudur
        System.out.println(b+  " " +b2);



    }
}
