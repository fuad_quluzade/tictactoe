package az.course.tdd.tictactoe.contract;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Student {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static void main(String[] args) {

        Student s=new Student();
        s.id="1234";
        // s.equals(s) reflexivity
        Student s1=new Student();
        s1.id="1234";

//        s.equals(s1) == s1.equals(s) simmetrik

        Student s2=new Student();
        s2.id="1234";

//        s.equals(s1) == true
//        s1.equals(s2) ==true
//        s.equals(s2) ==true   transitivity
        if(s.equals(s1)){
            System.out.println("false");
        }

        Set set=new HashSet();
        set.add(s);
        set.add(s1);
        System.out.println("set sizw is = " + set.size() );
    }
}
