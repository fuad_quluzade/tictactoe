package az.course.tdd.tictactoe.validations;

public class PhoneNumberValidation {

    public boolean test(String phoneNumber) {
        return phoneNumber.startsWith("+994") && phoneNumber.length()==13;
    }
}
