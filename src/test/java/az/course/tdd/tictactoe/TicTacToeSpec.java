package az.course.tdd.tictactoe;

import az.course.tdd.tictactoe.model.GameStep;
import az.course.tdd.tictactoe.repository.GameStepRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import az.course.tdd.tictactoe.spec.TicTacToe;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TicTacToeSpec {

    @Mock
    private GameStepRepository repository;

    @Captor
    private ArgumentCaptor<Long> captor;


    private TicTacToe ticTacToe;

    @BeforeEach
    void init() {
        ticTacToe = new TicTacToe(repository);
    }

    @Test
    void whenXIsOutsideTheBoardThenException() {
        int x = -1;
        int y = 0;
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside the board");
    }

    @Test
    void whenYIsOutsideTheBoardThenException() {
        int x = 1;
        int y = 5;
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside the board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
//      //Act
        ticTacToe.play(2, 1);
//      //Assert
        assertThatThrownBy(() -> ticTacToe.play(2, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("board is occupied");

        GameStep gameStepX=new GameStep();
        gameStepX.setPlayer('X');
        gameStepX.setX(2);
        gameStepX.setY(1);
        verify(repository,times(1)).save(gameStepX);

        GameStep gameStepO=new GameStep();
        gameStepO.setPlayer('X');
        gameStepO.setX(2);
        gameStepO.setY(1);
        verify(repository,times(1)).save(gameStepO);
    }

    @Test
    public void whenOccupiedThenRuntimeException1() {
        ticTacToe.play(2, 1); //X
        ticTacToe.play(2, 3);//O

    }

    @Test
    void whenStartThenNextPlayerX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    void givenLastTurnWasXWhenNextPlayerThenO() {
        ticTacToe.play(1, 1);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
    }

    @Test
    void givenLastTurnWasOWhenNextPlayerThenX() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 3);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void whenPlayThenNoWinner() {
        assertThat("no winner").isEqualTo(ticTacToe.play(1, 1));
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
       assertThat(ticTacToe.play(3,1)).isEqualTo("X is winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        assertThat(ticTacToe.play(1,3)).isEqualTo("O is winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        assertThat(ticTacToe.play(3,3)).isEqualTo("X is winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        assertThat(ticTacToe.play(3,1)).isEqualTo("X is winner");
    }
    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
      assertThat(ticTacToe.play(3,2)).isEqualTo("The result is draw");
    }

    @Test
    public void getAllSteps() {
        //Arrange
        when(repository.findAll()).thenReturn(List.of(new GameStep(1,'X',1,1)));
        //Act
        List<GameStep> gameSteps = ticTacToe.getGameSteps();
        //Assert
        assertThat(gameSteps).isEqualTo(List.of(new GameStep(1,'X',2,2)));
    }

    @Test
    public void stepById() {
        //Arrange
       when(repository.findById(2l)).thenReturn(Optional.of(new GameStep(1,'X',2,2)));
        //Act
        GameStep gameSteps = ticTacToe.getGameStepDetails(2L);
        //Assert
        verify(repository,times(1)).findById(captor.capture());
        System.out.println(captor.getValue());
        assertEquals(captor.getValue(),2l);
//        assertThat(gameSteps).isEqualTo(new GameStep(1,'X',2,2));
    }

}
