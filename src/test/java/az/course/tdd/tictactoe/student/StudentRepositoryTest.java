package az.course.tdd.tictactoe.student;

import az.course.tdd.tictactoe.model.Student;
import az.course.tdd.tictactoe.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private  StudentRepository studentRepository;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
        studentRepository.deleteAll();
    }

    @Test
    void itShouldCheckIsStudentExistByEmail(){
        //Given
        Student student=new Student(1,"Fuad","Quluzade","F@G.com");
        studentRepository.save(student);

        //When
        boolean b = studentRepository.existsByEmail("F@G.com");
        //Then
        assertThat(b).isTrue();
        
    }

    @Test
    void itShouldCheckIsStudentDoesNotExistByEmail(){
        //Given
        String email="F@G.com";

        //When
        boolean b = studentRepository.existsByEmail(email);
        //Then
        assertThat(b).isFalse();

    }
}